# Modules de Formations

## Module 1

|| Module 1|
|---|---|
| __Acquis à l'issue de Formation__ | Niveau sortie d'école ingénieur dans la compréhension de la programmation orientées objet et complexité algorithmique	|
| __Détail du module__ | <ul><li>Compréhension de la complexité algorithmique</li><li>Les proprietés,  Getter et setter</li><li>Ref Type / Value Type/ struct et class</li><li>Abstarction / Interface</li><li>Mutable And Immutable Class</li><li>La sérialisation</li><ul> |
| __Sources__ |	<ul><li>3.Creating Types in C# - C8 in a nutshell</li><li>17.serialization - C8 in a nutshell</li><li>A revoir eventuellement: https://h-deb.clg.qc.ca/Sujets/Maths/Complexite-algorithmique.html </li><ul> |
| __Modalités pédagogiques__ | Lecture de références/doc sur le sujet |
| __Volume horaire__ | ###	|
| __Evaluation__ | QCM via Coding game & Jury expert |
| __Principales questions__ | <ul><li>C'est quoi O(1), O(N)</li><li>complexité de max dans un tableau non trié</li><li>complexité de la recherche d'un element dans un tableau déjà trié: log N: expliquer pourquoi cette valeur : méthode diviser pour reigner</li><li>Tout hérite de la class object ? et par la suite Exercice de décompilation des principales méthodes de la classe object.  ie toString Equals, getHashcode</li><li>ref type vs value type</li><li>getter/setter dans le c#</li><li>struct vs class</li><li>Mutable And Immutable Class</li><li>interface vs abstract class</li><li>boxing vs unboxing</li><li>upcasting vs downcasting</li><li>Pourquoi effectuer la sérialisation d'un objet</li></ul>|
| __Esprit du Kata__ | coding game : A hérite de B et ordre d'appel des constructeur/destructeur <br> puis A test = new B() avec un writeln dans le constructeur/ destructeur de B et de A|
| __Reference Exercice__ | [Chapitre_1](Exercices/Chapitre_1.md) |


## Module 2

|| Module 2|
|---|---|
| __Acquis à l'issue de Formation__ | Usage environnement de développement<br>Niveau fondamental de comprehénsion du contexte d'éxécution C#<br>(code managé > compilateur > MSIL > CLR > Code machine) |
| __Détail du module__ | <ul><li>Contexte d'exécution</li><li>Manipulation de l'IDE VS</li><li>Structure et fichiers d'un projet .Net</li><li>Resharper</li></ul>|
| __Sources__ |	https://docs.microsoft.com/fr-fr/visualstudio/get-started/visual-studio-ide?view=vs-2019 |
| __Modalités pédagogiques__ | Insatller visual studio et resharper! |
| __Volume horaire__ | ### |
| __Evaluation__ | QCM via Coding game & Jury expert |
| __Principales questions__ | <ul><li>integrer une dll dans une solution</li><li>expliquer les différents fichiers générés après build</li><li>mode debug</li><li>mode release</li><li>calculer et comparer les performences d'un traitement</li><li>parcourir et comprendre la liste des méthodes d'une classe donnée</li><li>remonter dans la recherche de class fille a classe mere: a partir de la class dictionnary par exemple</li><li>resharper: prise en main</li></ul> |
| __Esprit du Kata__ | Eventuellement partage d'écran du candidat avec création de projet from scratch<br> et explication des différents fichiers générés.<br>Demande d'explication sur le contexte d'exécution<br>decompiler une methode tostring ou equals |
| __Reference Exercice__ | [Chapitre_2](Exercices/Chapitre_2.md) |

## Module 3

|| Module 3|
|---|---|
| __Acquis à l'issue de Formation__ | Maîtrise des collections  + tuples.<br>Savoir les différencier et les rapporter aux bons cas d'usage.|
| __Détail du module__ | <ul><li>Les différents types de collection</li><li>La structure interne de collection</li><li>Le hashcode d'un objet</li><li>Utilisation de hashcode dans une collection</li></ul> |
| __Sources__ |	7. Collections - C8 in a nutshell<br>https://www.decodejava.com/csharp-string-intern-pool.htm |
| __Modalités pédagogiques__ | A voir avec Wassim. Kata ? |
| __Volume horaire__ | ### |
| __Evaluation__ | Commit du Kata<br> code review Jury expert|
| __Principales questions__ | <ul><li>identifier la bonne collection à utliser si : beaucoup d'insertion et peu de recherche et si l'inverse</li><li>c'est quoi un tuple</li><li>L'utilité du hashcode. Est ce qu'il garantie l'égalité des objets comparés</li><li>hashset et structure interne d'un dictionnaire</li><li>dictionnary avec tuple comme clé</li><li>utilisation de foreach et comment ça fonctionne concretement en background cette instruction</li><li>shallow copy vs deep copy d'une structure de données</li><li>string intern pool|
| __Esprit du Kata__ | dictionnary avec key de type nouvelle class et en  implémentant la méthode equals.<br>Dans equals est ce qu'il suffit d'implementer le hashcode uniquement ? |
| __Reference Exercice__ | [Chapitre_3](Exercices/Chapitre_3.md) |

## Module 4

|| Module 4|
|---|---|
| __Acquis à l'issue de Formation__ | Formation initiale au asynchronisme / Tasks / multi-threading.+ multi process Niveau exo pratique et compréhension du chapitre 14 de C# in a nutshell de Albahari/Johansenn |
| __Détail du module__ | <ul><li>Les notions de base de thread, task, parallele.foreach</li><li>Le await/async</li><li>Les locks</li><li>Les collections threadsafe</li></ul>|
| __Sources__ |	https://www.e-naxos.com/Blog/post/De-la-bonne-utilisation-de-AsyncAwait-en-C.aspx<br> https://fdorin.developpez.com/tutoriels/csharp/threadpool/part1/#LI |
| __Modalités pédagogiques__ | 2 Katas et Lecture de références/doc	 |
| __Volume horaire__ | ### |
| __Evaluation__ | Commit des Katas, QCM via Coding game  & Jury expert |
| __Principales questions__ | <ul><li>Expliquer comment asynchronisme != multithreading</li><li>les outils de chaque mécanisme</li><li>Comment protéger les data partagés</li><li>C'est quoi une collection threadsafe ?</li><li>Par defaut, les collections en c# le sont-elles ?</li><li>Queue vs concurentQueue</li></ul> |
| __Esprit du Kata__ | Plusieurs solutions pour le problème de factorielle:<ul><li>Avec Task</li><li>Avec Thread</li><li>Avec Async Await  TODO</li><li>Parallele.foreach</li></ul> |
| __Reference Exercice__ | [Chapitre_4](Exercices/Chapitre_4.md) |

## Module 5

|| Module 5|
|---|---|
| __Acquis à l'issue de Formation__ | Introduction à LINQ lecture écriture  (recherche - tri – spliter une list par une certaine clé) |
| __Détail du module__ | <ul><li>Utilisation basique de LINQ</li><li>Problèmes connus de performances</li><li>Optimisation de l'utilisation de LINQ</li><li>Introduction à la EF</li></ul> |
| __Sources__ |	https://www.c-sharpcorner.com/article/introduction-to-linq/ |
| __Modalités pédagogiques__ | Kata.Création d'une classe<br> Utilisations de methodes / requête LINQ<br>lectures de références/doc sur sujet. |
| __Volume horaire__ | ### |
| __Evaluation__ | Commit du Kata et code review Jury expert |
| __Principales questions__ | <ul><li>C'est quoi LINQ</li><li>Lazy Loading vs Eager Loading</li><li>Utiliser le ToList en début ou à la fin de l'instruction ? pourquoi ?</li><li>EF is an application oriented view of database</li><li>LINQ is a database oriented view of database |
| __Esprit du Kata__ | A partir d'une liste ou table dans une BD préremplie effectuer des requêtes de recherches, de tri et spliter par une certaine clé |
| __Reference Exercice__ | [Chapitre_5](Exercices/Chapitre_5.md) |

## Module 6

|| Module 6|
|---|---|
| __Acquis à l'issue de Formation__ | Introduction au fonctionnement du garbage collector<br> Compréhension du fonctionnemet du Garbage collector et de l'interface IDisposable |
| __Détail du module__ | <ul><li>Fonctionnement d'un GC</li><li>Le LOH</li><li>Les générations dans GC</li><li>Le IDisposable</li></ul> |
| __Sources__ |	12. Disposal and Garbage Collector - C8 in a nutshell |
| __Modalités pédagogiques__ | Kata et Lecture de références/doc |
| __Volume horaire__ | ### |
| __Evaluation__ | QCM via Coding game & Jury expert |
| __Principales questions__ | <ul><li>libération de ressource managé</li><li>Libération de res__Sources__ unmanaged</li><li>nombre de génération</li><li>expliquer le mécanisme de fonctionnement</li><li>c'est quoi un LOH Large Object Heap</li><li>comment garantir la meilleur performence : en faisant appel à GC.collect ou on le laissant s'exécuter automatiquement</li></ul> |
| __Esprit du Kata__ | Implémenter l'interface IDisposable |
| __Reference Exercice__ | [Chapitre_6](Exercices/Chapitre_6.md) |
