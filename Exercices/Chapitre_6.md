﻿# Exercice Module 6

## Objectif

Rendre une classe ressouce iDisposable
Et faire appel a cette classe dans le main.

## Attendus:

A partir de l’exemple avec using sur une class qui n’hérite pas IDisposable
Voir si le candidat comprend le mot clé using et son intérêt.

Valider les modifications nécessaires:
* La classe doit hériter du IDisposable
* Il faut implémenter la méthode Dispose

## Implémentation

```csharp
using System;

namespace Chapitre_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World IDisposable!");

            using (Resources res = new Resources())
            {
                //do Traitement
                string test = res.ToString();
            }
        }
    }

    public class Resources : IDisposable
    {
        private bool disposed = false;
        public Resources()
        {

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if(!disposed)
            {
                if (disposing)
                {
                    // managed
                }

                // unamanged 
                disposed = true;
            }
        }

        ~Resources()
        {
            Dispose(false);
        }

    }
}
```