﻿# Exercice Module 3

## objectif

Personnaliser le fonctionnement d'un dictionnaire

* Implémentation d’un dictionnaire avec clé un tuple <int, une classe Personne> (attribut nom prénom date de naissance ville) et une valeur pays.
* Implémenter la méthode hash et la méthode equals

## Attendus:

Demander au candidat ce qu’on doit faire pour assurer le bon fonctionnement du dictionnaire (en précisant que l’identifiant est personne avec nom prénom et date de naissance uniquement !)

- Dans equals est ce qu'il suffit d'implementer le hashcode uniquement ? NON
- mutable vs immutable avec la notion de tuple
- Générer du hashcode avec des champs immutable car avec des champs mutables on risque de perdre l'info de départ
- Complexité d’une recherche dans une collection

## Implementation

```csharp
using System;
using System.Collections.Generic;

namespace Chapitre_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            Dictionary<Tuple<int, Personne>, Pays> dicPersonnes = new Dictionary<Tuple<int, Personne>, Pays>();
            dicPersonnes.Add( new Tuple<int, Personne>( 1, new Personne 
            { Nom = "P1", Prenom="P2", dateNaissance=new DateTime(1970,01,01), villeNaissance="Bordeaux" } 
            ), new Pays { Nom = "France" });

            dicPersonnes.Add(new Tuple<int, Personne>(1, new Personne
            { Nom = "P1", Prenom = "P2", dateNaissance = new DateTime(1970, 01, 01), villeNaissance = "Paris" }
            ), new Pays { Nom = "France" });

            dicPersonnes.Add(new Tuple<int, Personne>(1, new Personne
            { Nom = "P2", Prenom = "P2", dateNaissance = new DateTime(1970, 01, 01), villeNaissance = "Paris" }
            ), new Pays { Nom = "France" });

            Console.WriteLine($"Count = {dicPersonnes.Count}");
        }
    }

    public class Personne
    {
        public string Nom;
        public string Prenom;
        public DateTime dateNaissance;
        public string villeNaissance;

        public override bool Equals(object obj)
        {
            Personne personne = obj as Personne;
            if (personne == null)
                return false;

            if (Nom.Equals(personne.Nom)
                && Prenom.Equals(personne.Prenom)
                && dateNaissance.Equals(personne.dateNaissance))
                return true;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return Nom.GetHashCode() & Prenom.GetHashCode() & dateNaissance.GetHashCode();
        }
    }

    public class Pays
    {
        public string capitale;
        public string Nom;
    }
}
```