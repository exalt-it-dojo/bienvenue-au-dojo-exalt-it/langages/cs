﻿# Exercice Module 1

## Enoncé

Donner le resultat de l'affichage du code suivant

```csharp
using System;

namespace Chapitre_1
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                Input testStruct = new Input();
                testStruct.i = 1;

                Alpha objAlpha = new Alpha();
                objAlpha.Traitement(testStruct);

                Console.WriteLine($"testStruct value =  {testStruct.i}");

            }
        }
    }

    public class Alpha
    {
        public static Beta objBeta = new Beta();
        public Gamma objGamma = new Gamma();
        static Alpha() 
        {
            Console.WriteLine("description x: ");
        }

        public Alpha()
        {
            Console.WriteLine("description y: ");
        }

        public void Traitement(Input i_input)
        {
            i_input.i = 2;
            Console.WriteLine("description t:");
        }
    }

    public class Gamma
    {
        public Gamma()
        {
            Console.WriteLine("description g: ");
        }
    }

    public class Beta
    {
        public Beta()
        {
            Console.WriteLine("description a: ");
        }
    }

    public struct Input
    {
        public int i;
    }
}
```

## Attendus

* L'ordre de création + passage par copie pour la struct (value type) ce qui est différent pour les classes (ref type)

1. Attributs statiques
2. Puis Constructeur statique
3. Puis attribut non static
4. Puis constructeur par défaut
