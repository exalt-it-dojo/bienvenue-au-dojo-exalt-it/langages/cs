﻿# Exercice Module 5

## Objectif
A partir de la liste des process en cours d'execution afficher la liste des service contenant svchost, classé par ordre alphabétique croissant.

## Attendus

Focus sur le fait qu’on peut interroger n’importe quelle collection implémentant l’interface IEnumerable
+ proposer de remplacer la commande toList par toDictionnary avec utilisation de lambda expression pour identifer le key

## Implementation

```csharp
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Chapitre_5
{
	class Program
	{
		static void Main(string[] args)
		{
			var query = from process in System.Diagnostics.Process.GetProcesses()
						where !process.ProcessName.Contains("svchost")
						orderby process.ProcessName ascending
						select process;

			foreach (var currentItem in query.ToList())
			{
				Console.WriteLine(currentItem.ProcessName);
			}

			Console.Read();
		}
	}
}
```