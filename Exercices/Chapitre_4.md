﻿# Exercices Module 4

## Objectif
Implementer une fonction factoriel avec chaque méthode suivante:
* Task
* Thread
* Async
* Parallel.foreach

Optimiser du temps de calcul en réutilisant les données déjà calculées.

## Attendus

Focus sur:

- différence entre thread et task
- indiquer que chaque thread il a sa propre stack et contexte d'exécution
- chaque thread qui est en vie : ses variables en vie, avec N thread on n N callstack qui est en vie
- gestion des deadlock

## Implementations

### Async

```csharp
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Factorielle
{
    class Calcul
    {
        private Double m_resPair = 1;
        private Double m_resImPair = 1;

        public Double Res = 1;

        public async void CalculAsync(Double input)
        {
            DoComputationForPairAsync(input);
            DoComputationForImPairAsync(input);

            Res = m_resPair * m_resImPair;
            Console.WriteLine("Calcul Async {0}", Res);
        }

        public async Task DoComputationForPairAsync(Double end)
        {
            Double m_resPair = 1;
            await Task.Run(() =>
            {
                for (var value = 1; value <= end; value += 1)
                {
                    if (value % 2 == 0)
                        m_resPair *= value;
                }

                return m_resPair;
            });

            Console.WriteLine("DoComputationFor PairAsync {0}", m_resPair);
        }

        public async Task DoComputationForImPairAsync(Double end)
        {
            await Task.Run(() =>
            {
                for (var value = 1; value <= end; value += 1)
                {
                    if (value % 2 == 1)
                        m_resImPair *= value;
                }

                return m_resImPair;
            });

            Console.WriteLine("DoComputationFor ImPairAsync {0}", m_resImPair);
        }
    }
}
```

### ParalleleExecution

```csharp
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Factorielle
{
    class ParalleleExecution
    {

        public static void traitement(Double input)
        {
            Dictionary<int, Double> dictEntree = new Dictionary<int, Double>();
            dictEntree.Add(10, 0);
            dictEntree.Add(20, 0);
            dictEntree.Add(30, 0);

            Parallel.ForEach(dictEntree.Keys, key => dictEntree[key]  = Program.Recursive(key));

            foreach(var item in dictEntree)
            {
                Console.WriteLine("Calcul Parallel.ForEach Recursive  Key {0} Value {1}", item.Key, item.Value);
            }
            
        }
    }
}
```

### Task

```csharp
using System;
using System.Threading.Tasks;

namespace Factorielle
{
    class Program
    {
        static void Main(string[] args)
        {
            Double inputValue = 30;

            var watch = System.Diagnostics.Stopwatch.StartNew();
            double resRec = Recursive(inputValue);
            Console.WriteLine("resRec {0}", resRec);
            watch.Stop();
            Console.WriteLine($"Execution Time recursive: {watch.ElapsedMilliseconds} ms");
            watch.Reset();

            watch = System.Diagnostics.Stopwatch.StartNew();
            Task<Double>[] taskArray = { Task<Double>.Factory.StartNew(() => DoComputationForPair(inputValue)),
                                     Task<Double>.Factory.StartNew(() => DoComputationForImPair(inputValue)) };


            var results = new Double[taskArray.Length];

            Double result = 1;

            for (int i = 0; i < taskArray.Length; i++)
            {
                results[i] = taskArray[i].Result;
                Console.Write("Task {0}: {1:N1} {2}", i, results[i],
                                  i == taskArray.Length - 1 ? "= " : "* ");
                result *= results[i];
            }
            Console.WriteLine("Task {0}", result);
            watch.Stop();
            Console.WriteLine($"Execution Time tasks: {watch.ElapsedMilliseconds} ms");

            if (resRec == result)
                Console.WriteLine("les deux resultat sont égaux " );

            // use of thread
            ThreadPoolExemple thExemple = new ThreadPoolExemple();
            thExemple.Traitement(inputValue);

            // parallele
            ParalleleExecution.traitement(inputValue);

            //use async await

            //Calcul _calc = new Calcul();

            //_calc.CalculAsync(inputValue);

            //if (resRec == result && resRec == _calc.Res)
            //   Console.WriteLine("les 3 resultats sont égaux ");

            Console.ReadKey();

        }


        public static  Double DoComputationForPair(Double end)
        {
            Double res = 1;
            for (var value = 1; value <= end; value += 1)
                if(value % 2 == 0)
                    res *= value;

            return res;
        }

        public static  Double DoComputationForImPair(Double end)
        {
            Double res = 1;
            for (var value = 1; value <= end; value += 1)
                if (value % 2 == 1)
                    res *= value;

            return res;
        }

        public static Double Recursive(double end)
        {
            if (end == 1)
                return 1;
            else
                return end * Recursive(end - 1);
        }
    }
}
```

## Thread

```csharp
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Factorielle
{
    class ThreadPoolExemple
    {

        // dans ce cas les attributs sont utilisés séparement dans le calcul
        // si on est ramené à partager ces attributs dans plusieurs threads à la fois il faut les protéger avec les locks
        private Double m_resPair = 1;
        private Double m_resImPair = 1;

        public void Traitement(double input)
        {
            Thread th1 = new Thread(() => {
                m_resPair = Program.DoComputationForPair(input);
                Console.WriteLine("Thread 1");
            });

            Thread th2 = new Thread(() => {
                m_resImPair = Program.DoComputationForImPair(input);
                Console.WriteLine("Thread 2");
            });

            th1.Start();
            th2.Start();

            th1.Join();
            th2.Join();

            Console.WriteLine("Calcul ThreadPoolExemple {0}", m_resImPair * m_resPair);

        }
    }
}
```