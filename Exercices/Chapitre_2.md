# Exercice Module 2

## Objectif

Compréhension de l'environnement d'execution et de dévelopement.

__Projet exemple dans le dossier Chapitre_2__

## Attendus

Partage d’écran et création d’une nouvelle solution et parcours des différents dossiers/fichiers créés

* C’est quoi une solution sln
* Le contenu d’un fichier csproj – explication des balises xml présentes dans le fichier
* Débugger du code
* C'est quoi le contenu du dossier bin
* C’est quoi le contenu du dossier obj
* Citer les méthodes présentes dans tous object toString, Equals et GetHashcode()
* decompiler l'assembly
* mettre en forme de tableau – comprendre l’intérêt !